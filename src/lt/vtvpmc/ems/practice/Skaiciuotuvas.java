package lt.vtvpmc.ems.practice;

import java.util.Scanner;

public class Skaiciuotuvas {
	static Scanner sc = new Scanner(System.in);

	public static void main(String[] args) {
		sc = new Scanner(System.in);
		System.out.println("Ivesk pirma skaiciu: ");
		int a = sc.nextInt();
		System.out.println("Ivesk antra skaiciu: ");
		int b = sc.nextInt();
		System.out.println("Pasirink operanda(+ - * / = ^)");
		char operandas = sc.next().charAt(0);
		switch (operandas) {
		case '+':
			System.out.println("Resultatas: " + (a + b));
			break;
		case '-':
			System.out.println("Rezultatas: " + (a - b));
			break;

		case '*':
			System.out.println("Rezultatas: " + (a * b));
			break;

		case '/':
			System.out.println("Rezultatas: " + (a / b));
			break;

		case '=':
			System.out.println("Rezultatas: " + Math.sqrt(a));
			break;

		case '^':
			System.out.println("Rezultatas: " + Math.pow(a, b));
			break;

		default:
			break;
		}
	}

}
